const fileReader = require("./fileReader");
const shopifyService = require("./shopify");
const helper = require("./helper");
const fs = require("fs");
const config = require("./config");

let shopifyDeleteOrder = async () => {
  console.log("start deleting order process..............\n")
  
  let allOrder = await getAllOrderFromDatabase()
  if (!allOrder.status) {
    console.log("Error while getting orders from json .............\n")
    return false
  }

  let orders = allOrder.data;

  let i = 0
  await deleteScript(orders, i);

  return true;
}

let deleteScript = (orders, i) => {
  return new Promise(async (resolve, result) => {

    if (i > orders.length - 1) {
      // process completed
      console.log("--------------- PROCESS COMPLETED  --------------------- \n")
      return resolve({ status: true, message: "Process completed" });
    }
    
    let order = orders[i];
    console.log(order.orderId)

    console.log("--------------- NEW ORDER PROCESS STARTED : " + order.name + "- ---------------------");
    let deleteOrder = await shopifyService.deleteOrder(order.orderId);
    if(deleteOrder.status){
      console.log("--------------- ORDER DELETED : " + order.name + "---------------------");
      await deleteOrderInMysql(order.id);
    } else {
      console.log("--------------- ERROR WHILE DELETED ORDER FROM SHOPIFY ---------------------");  
    }
    
    console.log("--------------- CONTINUE WITH OTHER ORDER : " + order.name + " --------------------- \n");

    return resolve(deleteScript(orders, (i + 1)));
  })
}

let insertOrderInMYSQL = (orderId, orderNumber, orderName, environment,) => {
  return new Promise((resolve) => {
    let sql = `INSERT INTO orders (orderId, order_number, name, environment) values (?, ?, ?, ?);`;
    let params = [orderId, orderNumber, orderName, environment];
    
    let conn = require("./mysql")
    conn.query(sql, params, (err, results) => {
      if (err) {
        console.log("mysql error");
        return resolve({ status: false });
      }
      return resolve({ status: true });

    });

  })
}

let getAllOrderFromDatabase = () => {
  return new Promise((resolve) => {
    let sql = `SELECT * FROM freshmealplan.orders where id >= 77 and id <= 175 and environment = ? and isDeleted = 0`;
    let params = [config.ENVIRONMENT];
    
    let conn = require("./mysql")
    conn.query(sql, params, (err, results) => {
      if (err) {
        return resolve({ status: false });
      }
      return resolve({ status: true, data: results });
    });

  })
}

let deleteOrderInMysql = (id) => {
  return new Promise((resolve) => {
    let sql = `UPDATE orders SET isDeleted = 1 where id = ?;`;
    let params = [id];
    
    let conn = require("./mysql")
    conn.query(sql, params, (err, results) => {
      if (err) {
        console.log("mysql error");
        return resolve({ status: false });
      }
      return resolve({ status: true });

    });

  })
}



let getOrderFromMysql = (orderNumber, environment,) => {
  return new Promise((resolve) => {
    let sql = `SELECT * FROM orders where name = ? and environment = ? AND isDeleted = 0;`;
    let params = [orderNumber, environment];
    
    let conn = require("./mysql")
    conn.query(sql, params, (err, results) => {
      if (err) {
        return resolve({ status: false });
      }
      return resolve({ status: true, data: results });

    });

  })
}

shopifyDeleteOrder()
