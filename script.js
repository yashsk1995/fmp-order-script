const fileReader = require("./fileReader");
const shopifyService = require("./shopify");
const helper = require("./helper");
const fs = require("fs");
const config = require("./config");
let fileName = config.fileName
let allOrder = {}

let getAllJsonOrder = () => {
  let allOrderJsonFile = config.CSV_FILE_ALL_ORDER_STAGING
  if (config.ENVIRONMENT == "production") {
    allOrderJsonFile = config.CSV_FILE_ALL_ORDER_PRODUCTION
  }

  let readJsonAllOrder = fs.readFileSync(`./jsons/${allOrderJsonFile}.json`);
  if (!readJsonAllOrder) {
    console.log("Error while get all order");
    return false;
  }

  let allOrder = JSON.parse(readJsonAllOrder.toString())
  if (!allOrder) {
    console.log("Error while get all order");
    return false
  }

  return allOrder
}


if (fs.existsSync(`./errors/${fileName}.error.json`)) fs.unlinkSync(`./errors/${fileName}.error.json`);
if (fs.existsSync(`./success/${fileName}.success.json`)) fs.unlinkSync(`./success/${fileName}.success.json`);
if (fs.existsSync(`./jsons/${fileName}.json`)) fs.unlinkSync(`./jsons/${fileName}.json`);
if (fs.existsSync(`./deleted-orders/deletedOrder.json`)) fs.unlinkSync(`./deleted-orders/deletedOrder.json`);

let shopifyOrder = async () => {
  console.log("start process of reading file data")
  let readenData = await fileReader(fileName);
  if (!readenData.status) {
    return false;
  }

  console.log("End process of reading file data..............\n")

  let data = readenData.data;

  console.log("start process of converting order object in array order format..............\n")
  let formatData = helper.convertObjInArray(data);
  console.log("end process of converting order object in array order format..............\n")

  allOrder = getAllJsonOrder()
  if (!allOrder) {
    console.log("Error while getting orders from json .............\n")
    return false
  }

  if (config.ENVIRONMENT == "production") {
    console.log("--------------------------------------------  PRODUCTION SCRIPT START  --------------------------------------------")
    let startProcess = await startOrderProcessProduction(formatData, i)
    console.log(startProcess.message);
    console.log("order process completed")
  } else {
    console.log("--------------------------------------------  STAGING SCRIPT START  --------------------------------------------")
    let startProcess = await startOrderProcess(formatData, i)
    console.log(startProcess.message);
    console.log("order process completed")
  }

  return true;
}

let startOrderProcess = (orders, i) => {
  return new Promise(async (resolve, result) => {

    if (i > orders.length - 1) {
      // process completed
      return resolve({ status: true, message: "Process completed" });
    }

    console.log(orders[i].order.customer);

    let order = orders[i].order
    let orderNumber = order.name;

    let existOrderData = allOrder[orderNumber];
    if (existOrderData) {
      let orderIds = existOrderData.map(x => x.id);
      let getOrder = await shopifyService.getOrderByOrderIds(orderIds);
      if (getOrder.status && getOrder.data && getOrder.data.length > 0) {
        let findOrders = getOrder.data;
        for (let index = 0; index < findOrders.length; index++) {
          const element = findOrders[index];
          if (parseInt(element.total_price) == 0) {
            // need to delete order
            let orderObj = { "orderId": element.id, "price": element.total_price, orderNumber: orderNumber }
            await helper.saveDeleteOrder("deletedOrder", orderObj);

            await shopifyService.deleteOrder(element.id);
          }
        }
      }
    }

    let getOrderAdded = await getOrderFromMysql(order.name, config.ENVIRONMENT);
    if (getOrderAdded.data && getOrderAdded.data.length > 0) {
      console.log(" -----------------------------------------ORDER ALREADY CREATED SKIP ORDER : " + order.name + " -----------------------------------------");
      return resolve(startOrderProcess(orders, (i + 1)));
    }

    console.log("start order processing for order : " + order.name + "\n")

    let customerEmail = order.customer.id;
    let getCustomer = await shopifyService.searchCustomers(customerEmail);
    if (!getCustomer.status) {
      // need to save in false response
      let errObj = { "error_type": "invalid_customer", "email": customerEmail, "order_number": order.name }
      await helper.saveErrorOfOrder(fileName, order.name, errObj);

      console.log("Error while retriving customer for order : " + order.name + "\n")

      // continue with new order
      console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
      return resolve(startOrderProcess(orders, (i + 1)));

    }

    order.customer.id = getCustomer.data.id;
    let orderDiscount = (order.order_discount && parseFloat(order.order_discount) > 0) ? parseFloat(order.order_discount) : 0;
    let discount = 0
    let productDiscount = 0
    if (orderDiscount > 0) {
      console.log(orderDiscount);
      let subTotal = parseFloat(order.total_price) - orderDiscount;
      let totalQuantity = order.line_items.map(x => { return (x.quantity ? parseInt(x.quantity) : 0) }).reduce((prev, curr) => prev + curr, 0)
      let productDiscount = (subTotal / totalQuantity);
      productDiscount = parseFloat(productDiscount.toFixed());
      console.log(productDiscount);
    }

    // get productId of line Items
    for (let index = 0; index < order.line_items.length; index++) {
      const element = order.line_items[index];
      let variant_sku = element.sku;
      let variant_title = element.variant_title;
      let lineItem_title = element.name

      let getProduct = await shopifyService.getProductFromProductTitle(lineItem_title);
      if (!getProduct.status || !getProduct.data) {
        // need to save in false response
        let errObj = { "error_type": "invalid_product", sheetData: { variant_title, variant_sku, lineItem_title }, "order_number": order.name }
        await helper.saveErrorOfOrder(fileName, order.name, errObj);

        console.log("Error while retriving product for order : " + order.name + " product : " + lineItem_title + "\n")

        // continue with new order
        console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
        return resolve(startOrderProcess(orders, (i + 1)));

      }

      let productData = getProduct.data
      let variantData = helper.arrangeAllVariantsinArray(productData);

      if (variantData.length == 0) {
        let errObj = { "error_type": "variants_not_found", sheetData: { variant_title, variant_sku, lineItem_title }, "order_number": order.name }
        await helper.saveErrorOfOrder(fileName, order.name, errObj);

        console.log("Error product variant not found for order : " + order.name + " product : " + lineItem_title + "\n")

        // continue with new order
        console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
        return resolve(startOrderProcess(orders, (i + 1)));
      }

      let variants = variantData.filter((element) => {
        // if(element.sku == variant_sku && variant_title == element.title) {
        if (element.sku == variant_sku) {
          return element
        }
      })

      if (!variants || variants.length == 0 || (variants.length > 0 && (!variants[0].id || !variants[0].product_id))) {
        let errObj = { "error_type": "invalid_variant_id_or_product_id", data: variants, sheetData: { variant_title, variant_sku, lineItem_title }, "order_number": order.name }
        await helper.saveErrorOfOrder(fileName, order.name, errObj);

        console.log("Error while retriving product variant for order : " + order.name + "\n")

        // continue with new order
        console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
        return resolve(startOrderProcess(orders, (i + 1)));
      }

      order.line_items[index].id = variants.id;
      order.line_items[index].product_id = variants.product_id;
      order.line_items[index].price = (order.line_items[index].price - productDiscount);

    }

    // start order process

    let createOrder = await shopifyService.createOrder({ order: order });
    if (!createOrder.status) {
      // need to save in false response
      let errObj = { "error_type": "order_creation", "order_number": order.name }
      await helper.saveErrorOfOrder(fileName, order.name, errObj);

      console.log("Error while creating for order : " + order.name)

      // continue with new order
      return resolve(startOrderProcess(orders, (i + 1)));
    }
    let newOrder = createOrder.data
    console.log(order.total_price)
    console.log(newOrder.total_price)
    if (parseInt(order.total_price) != parseInt(newOrder.total_price)) {
      // need to save in false response
      let priceObj = { "order_name": order.name, "new_order_number": newOrder.order_number, id: newOrder.id, old_price: order.total_price, new_price: newOrder.total_price };
      await helper.savePriceIssue(fileName, priceObj);
      console.log("Created Order [price not same as order price.")
    }

    await insertOrderInMYSQL(newOrder.id, newOrder.order_number, order.name, config.ENVIRONMENT);

    console.log("complete order : " + order.name + "\n")

    await helper.saveSuccessOfOrder(fileName, order.name, newOrder.id);

    console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   COMPLETE ORDER : " + order.name + "    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    return resolve(startOrderProcess(orders, (i + 1)));

  })
}


let startOrderProcessProduction = (orders, i) => {
  return new Promise(async (resolve, result) => {
    if (i > orders.length - 1) {
      // process completed
      return resolve({ status: true, message: "Process completed" });
    }

    let order = orders[i].order

    let orderNumber = order.name;
    let existOrderData = allOrder[orderNumber];
    if (existOrderData) {
      let orderIds = existOrderData.map(x => x.id);
      let getOrder = await shopifyService.getOrderByOrderIds(orderIds);
      if (getOrder.status && getOrder.data && getOrder.data.length > 0) {
        let findOrders = getOrder.data;
        for (let index = 0; index < findOrders.length; index++) {
          const element = findOrders[index];
          if (parseInt(element.total_price) == 0) {
            // need to delete order
            let orderObj = { "orderId": element.id, "price": element.total_price, orderNumber: orderNumber }
            await helper.saveDeleteOrder("deletedOrder", orderObj);

            await shopifyService.deleteOrder(element.id);
          }
        }
      }
    }

    let getOrderAdded = await getOrderFromMysql(order.name, config.ENVIRONMENT);
    if (getOrderAdded.data && getOrderAdded.data.length > 0) {
      console.log(" -----------------------------------------ORDER ALREADY CREATED SKIP ORDER : " + order.name + " -----------------------------------------");
      return resolve(startOrderProcess(orders, (i + 1)));
    }

    console.log("start order processing for order : " + order.name + "\n")

    let customerEmail = order.customer.id;
    let getCustomer = await shopifyService.searchCustomers(customerEmail);
    if (!getCustomer.status) {
      // need to save in false response
      let errObj = { "error_type": "invalid_customer", "email": customerEmail, "order_number": order.name }
      await helper.saveErrorOfOrder(fileName, order.name, errObj);

      console.log("Error while retriving customer for order : " + order.name + "\n")

      // continue with new order
      console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
      return resolve(startOrderProcessProduction(orders, (i + 1)));

    }

    order.customer.id = getCustomer.data.id;
    let orderDiscount = (order.order_discount && parseFloat(order.order_discount) > 0) ? parseFloat(order.order_discount) : 0;
    let discount = 0
    let productDiscount = 0
    if (orderDiscount > 0) {
      console.log(orderDiscount);
      let subTotal = parseFloat(order.total_price) - orderDiscount;
      let totalQuantity = order.line_items.map(x => { return (x.quantity ? parseInt(x.quantity) : 0) }).reduce((prev, curr) => prev + curr, 0)
      let productDiscount = (subTotal / totalQuantity);
      productDiscount = parseFloat(productDiscount.toFixed());
      console.log(productDiscount);
    }

    // get productId of line Items
    for (let index = 0; index < order.line_items.length; index++) {
      const element = order.line_items[index];
      let variant_id = element.variant_id;
      let lineItem_title = element.name

      let getProduct = await shopifyService.getProductByVariant(variant_id);
      if (!getProduct.status || !getProduct.data) {
        // need to save in false response
        let errObj = { "error_type": "invalid_variant_id", sheetData: { variant_id }, "order_number": order.name }
        await helper.saveErrorOfOrder(fileName, order.name, errObj);

        console.log("Error while retriving variant for order : " + order.name + " product : " + lineItem_title + " varient : " + variant_id + "\n")

        // continue with new order
        console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
        return resolve(startOrderProcessProduction(orders, (i + 1)));

      }

      let variant = getProduct.data

      if (!variant || (variant && (!variant.id || !variant.product_id))) {
        // need to save in false response
        let errObj = { "error_type": "invalid_variant_or_product_id", variant, sheetData: { variant_id }, "order_number": order.name }
        await helper.saveErrorOfOrder(fileName, order.name, errObj);

        console.log("Error while retriving variant and product id for order : " + order.name + " product : " + lineItem_title + "\n")

        // continue with new order
        console.log(" -----------------------------------------SKIP ORDER : " + order.name + " -----------------------------------------");
        return resolve(startOrderProcessProduction(orders, (i + 1)));
      }

      order.line_items[index].id = variant.id;
      order.line_items[index].product_id = variant.product_id;
      order.line_items[index].price = (order.line_items[index].price - productDiscount);

    }

    console.log(order.name)
    // start order process
    let createOrder = await shopifyService.createOrder({ order: order });
    if (!createOrder.status) {
      // need to save in false response
      let errObj = { "error_type": "order_creation", "order_number": order.name }
      await helper.saveErrorOfOrder(fileName, order.name, errObj);

      console.log("Error while creating for order : " + order.name)

      // continue with new order
      return resolve(startOrderProcessProduction(orders, (i + 1)));
    }

    let newOrder = createOrder.data
    console.log(order.total_price)
    console.log(newOrder.total_price)
    if (parseInt(order.total_price) != parseInt(newOrder.total_price)) {
      // need to save in false response
      let priceObj = { "order_name": order.name, "new_order_number": newOrder.order_number, id: newOrder.id, old_price: order.total_price, new_price: newOrder.total_price };
      await helper.savePriceIssue(fileName, priceObj);
      console.log("Created Order [price not same as order price.")
    }

    await insertOrderInMYSQL(newOrder.id, newOrder.order_number, order.name, config.ENVIRONMENT);
    console.log("complete order : " + order.name + "\n")

    await helper.saveSuccessOfOrder(fileName, order.name, newOrder.id);

    console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   COMPLETE ORDER : " + order.name + "    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    return resolve(startOrderProcessProduction(orders, (i + 1)));

  })
}

shopifyOrder()


let insertOrderInMYSQL = (orderId, orderNumber, orderName, environment,) => {
  return new Promise((resolve) => {
    let sql = `INSERT INTO orders (orderId, order_number, name, environment) values (?, ?, ?, ?);`;
    let params = [orderId, orderNumber, orderName, environment];
    
    let conn = require("./mysql")
    conn.query(sql, params, (err, results) => {
      if (err) {
        console.log("mysql error");
        return resolve({ status: false });
      }
      return resolve({ status: true });

    });

  })
}

let getOrderFromMysql = (orderNumber, environment,) => {
  return new Promise((resolve) => {
    let sql = `SELECT * FROM orders where name = ? and environment = ? and isDeleted = 0;`;
    let params = [orderNumber, environment];
    
    let conn = require("./mysql")
    conn.query(sql, params, (err, results) => {
      if (err) {
        return resolve({ status: false });
      }
      return resolve({ status: true, data: results });

    });

  })
}