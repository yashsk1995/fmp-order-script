const axios = require('axios');
const config = require('./config');
let STORE_ACCESS_ID = config.SHOPIFY_STAGING_STORE_ACCESS_ID
let STORE_PASSWORD = config.SHOPIFY_STAGING_STORE_PASSWORD
let STORE_URL = config.SHOPIFY_STAGING_STORE_URL
let API_VERSION = config.SHOPIFY_STAGING_API_VERSION

if (config.ENVIRONMENT == "production") {
    STORE_ACCESS_ID = config.SHOPIFY_PRODUCTION_STORE_ACCESS_ID
    STORE_PASSWORD = config.SHOPIFY_PRODUCTION_STORE_PASSWORD
    STORE_URL = config.SHOPIFY_PRODUCTION_STORE_URL
    API_VERSION = config.SHOPIFY_PRODUCTION_API_VERSION
}



const BASE_URL = `https://${STORE_ACCESS_ID}:${STORE_PASSWORD}@${STORE_URL}/admin/api/${API_VERSION}/`;

console.log(BASE_URL);

module.exports = {

    getAllOrders: async (pageInfo, limit) => {
        const url = BASE_URL + `/orders.json?limit=${limit}&page_info=${pageInfo}`;
        return await axios.get(url);
    },

    getOrderDetails: async (orderId) => {
        const url = BASE_URL + `orders/${orderId}.json`;
        return await axios.get(url);
    },


    getProductList: async () => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + 'products.json?limit=250';
            axios.get(url).then((result) => {
                if (result && result.data && result.data.products) {
                    return resolve({ status: true, data: result.data.products })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                console.log(e);
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    getProductFromProductTitle: async (product_title) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `products.json?title=${encodeURIComponent(product_title)}`;
            axios.get(url).then((result) => {
                if (result && result.data && result.data.products && result.data.products.length > 0) {
                    console.log(product_title, " =>  done");
                    return resolve({ status: true, data: result.data.products })
                }
                console.log(product_title, " => failed");
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                console.log(e);
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    createOrder: async (order) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `orders.json`;
            axios.post(url, order).then((result) => {
                if (result && result.data && result.data.order) {
                    return resolve({ status: true, data: result.data.order })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                console.log(e);
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    getOrder: async (orderId) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `orders/${orderId}.json`;
            axios.get(url).then((result) => {
                if (result && result.data && result.data.order) {
                    return resolve({ status: true, data: result.data.order })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                console.log(e);
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    getOrderByOrderIds: async (orderId) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `orders.json?status=any&ids=${orderId.join(",")}`;
            axios.get(url).then((result) => {
                if (result && result.data && result.data.orders && result.data.orders.length > 0) {
                    return resolve({ status: true, data: result.data.orders })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                console.log(e);
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    deleteOrder: async (orderId) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `orders/${orderId}.json`;
            axios.delete(url).then((result) => {
                if (result) {
                    return resolve({ status: true, data: result.data.order })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                console.log("--------- ERROR WHILE DELETE SHOPIFY ORDER -----------");
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    getProductByVariant: async (variant_id) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `variants/${variant_id}.json`;
            axios.get(url).then((result) => {
                if (result && result.data && result.data.variant) {
                    return resolve({ status: true, data: result.data.variant })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    searchProductByVariant: async (searchQuery) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `variants/search.json?query=${searchQuery}`; // searchQuery = sku:XYZ000 
            axios.get(url).then((result) => {
                if (result && result.data && result.data.variant) {
                    return resolve({ status: true, data: result.data.variant })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    getOrderByNumber: async (orderNumber) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `orders.json?name=${orderNumber}&status=any&limit=1`;
            axios.get(url).then((result) => {
                if (result && result.data && result.data.customers && result.data.customers.length > 0) {
                    return resolve({ status: true, data: result.data.customers[0] })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                return resolve({ status: false, data: null, error: true })
            });
        })
    },

    searchCustomers: async (query) => {
        return new Promise((resolve, reject) => {
            const url = BASE_URL + `customers/search.json?query=${query}`;
            axios.get(url).then((result) => {
                if (result && result.data && result.data.customers && result.data.customers.length > 0) {
                    return resolve({ status: true, data: result.data.customers[0] })
                }
                return resolve({ status: false, data: null, error: false })
            }).catch((e) => {
                return resolve({ status: false, data: null, error: true })
            });
        })
    },


};