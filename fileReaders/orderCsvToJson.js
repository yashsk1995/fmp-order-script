const reader = require('xlsx')
const fs = require('fs');
const config = require("./../config")

let fileName = "all_order_staging"
if (config.ENVIRONMENT == "production") {
  fileName = "all_order_production";
}

const csvToJSON = (fileName) => {
  // return new Promise((resolve, reject) => {
  try {

    // Reading file
    if (!fs.existsSync(`./../csvs/${fileName}.csv`)) {
      console.log("error-----------------");
    }
    const file = reader.readFile(`./../csvs/${fileName}.csv`)

    // get all sheets
    const sheets = file.SheetNames;

    // get all rows of sheets
    let data = {};

    for (let i = 0; i < sheets.length; i++) {
      const temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]])
      if(config.ENVIRONMENT == "production"){
        temp.forEach((res, i) => {
          let orderNumber = res["Name"];
          if(!orderNumber){
            console.log("error")
          }
          orderNumber = (orderNumber && orderNumber.toString().indexOf("#") >= 0) ? (orderNumber).substr(1) : orderNumber;

          if (!data[orderNumber]) data[orderNumber] = [];
          if (orderNumber && res["ID"]) {
            data[orderNumber].push({
              "email": res["Email"],
              "id": res["ID"]
            })
          }
        })
      } else {
        temp.forEach((res, i) => {
          let orderNumber = res["Name"];
          if(!orderNumber){
            console.log("error")
          }
          orderNumber = (orderNumber && orderNumber.toString().indexOf("#") >= 0) ? (orderNumber).substr(1) : orderNumber;
          if (!data[orderNumber]) data[orderNumber] = [];
          if (orderNumber && res["Id"]) {
            data[orderNumber].push({
              "email": res["Email"],
              "id": res["Id"]
            })
          }
        })
      }
      

      
    }

    let countObj = {}
    for (const [key, value] of Object.entries(data)) {
      countObj[key] = value.length
      if (value.length > 1) {
        console.log(key, value.length, data[key].map(x => x.id))
      }
    }

    // console.log(countObj);

    fs.writeFileSync(`./../jsons/${fileName}.json`, JSON.stringify(data));
    // create a json file for backup;
    // resolve({ "status": true, message: "file successfully read.", data: ordersJSON });
    return;
  } catch (error) {
    console.log(error)
  }
}

csvToJSON(fileName)