const mysql = require('mysql');

const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root', /* MySQL User */
  password: '', /* MySQL Password */
  database: 'freshmealplan' /* MySQL Database */
});

// const conn = mysql.createConnection({
//   host: '35.244.6.139',
//   user: 'root', /* MySQL User */
//   password: 'root', /* MySQL Password */
//   database: 'freshmealplan' /* MySQL Database */
// });
   
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected with App...');
});

module.exports = conn
   