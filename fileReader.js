const reader = require('xlsx')
const fs = require('fs');
let UniqueTransactionId = new Date().getTime()


module.exports = (fileName) => {
  return new Promise((resolve, reject) => {
    try {

      // Reading file
      if (!fs.existsSync(`./csvs/${fileName}.csv`)) {
        return resolve({ "status": false, "message": "Invalid file" })
      }

      console.log("Process Progress..........")

      const file = reader.readFile(`./csvs/${fileName}.csv`)

      console.log("50% Progress completed")

      let data = [];

      // get all sheets
      const sheets = file.SheetNames;

      // get all rows of sheets
      let ordersJSON = {};
      let removeWithoutLineItemOrder = [];

      for (let i = 0; i < sheets.length; i++) {
        const temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]])
        let forward = true
        // temp.forEach((res, i) => {
        //     // console.log(res["Lineitem variant id"]);
        //     // arranging data order number wise
        //     if (!ordersJSON[res["Name"]]) {
        //       ordersJSON[res["Name"]] = {};
        //     }

        //     if (!ordersJSON[res["Name"]]["order"]) {
        //       ordersJSON[res["Name"]]["order"] = {};
        //     }

        //     if (checkMainOrderObj(res)) {
        //       ordersJSON[res["Name"]]["order"] = formatOrderObj(res, fileName);
        //     }

        //     if (!ordersJSON[res["Name"]]["order"]["line_items"]) {
        //       ordersJSON[res["Name"]]["order"]["line_items"] = []
        //     }
        //     ordersJSON[res["Name"]]["order"]["line_items"].push(formatLineItemObj(res));

        // })

        temp.forEach((res, i) => {

          if (!res["Lineitem variant id"]) {
            if (!removeWithoutLineItemOrder.includes(res["Name"])) { removeWithoutLineItemOrder.push(res["Name"]) }
          }

          if (!ordersJSON[res["Name"]]) {
            ordersJSON[res["Name"]] = {};
          }

          if (!ordersJSON[res["Name"]]["order"]) {
            ordersJSON[res["Name"]]["order"] = {};
          }

          if (checkMainOrderObj(res)) {
            ordersJSON[res["Name"]]["order"] = formatOrderObj(res, fileName);
          }

          if (!ordersJSON[res["Name"]]["order"]["line_items"]) {
            ordersJSON[res["Name"]]["order"]["line_items"] = []
          }
          ordersJSON[res["Name"]]["order"]["line_items"].push(formatLineItemObj(res));

        })
      }


      fs.writeFileSync(`./jsons/${fileName}.json`, JSON.stringify(ordersJSON));
      // create a json file for backup;
      resolve({ "status": true, message: "file successfully read.", data: ordersJSON, invalidData: removeWithoutLineItemOrder });
      return;
    } catch (error) {
      reject(error);
    }
  })
}

let formatOrderObj = (orderObj, fileName) => {
  let transactionId = addOneNumberINTransactionId();
  let parent_id = addOneNumberINTransactionId();

  let element = {
    "name": orderObj["Name"] + (["Shopify_National", "demo"].includes(fileName) ? "-NE" : null), // need to know
    "order_number": orderObj["Name"],
    "fulfillment_status": orderObj["Fulfillment Status"],
    "customer": {
      "id": orderObj["Email"], // need to know
    },
    "total_price": orderObj["Transaction Amount"],
    "phone": orderObj["Phone"],
    "shipping_address": {
      "first_name": orderObj["Shipping First Name"],
      "last_name": orderObj["Shipping Last Name"],
      "address1": orderObj["Shipping Address1"],
      "phone": orderObj["Shipping Phone"],
      "city": orderObj["Shipping City"],
      "province": orderObj["Shipping Province"],
      "country": orderObj["Shipping Country"],
      "zip": orderObj["Shipping Zip"]
    },
    "billing_address": {
      "first_name": orderObj["Billing First Name"],
      "last_name": orderObj["Billing Last Name"],
      "address1": orderObj["Billing Address1"],
      "phone": orderObj["Billing Phone"],
      "city": orderObj["Billing City"],
      "province": orderObj["Billing Province"],
      "country": orderObj["Billing Country"],
      "zip": orderObj["Billing Zip"]
    },
    "created_at": orderObj["Processed At"], // need to know
    "order_discount": orderObj["Discount Amount"],
    "transactions": [
      {
        "id": transactionId,
        "admin_graphql_api_id": "gid://shopify/OrderTransaction/" + transactionId,
        "amount": orderObj["Transaction Amount"],
        "authorization": "authorization-key",
        "created_at": orderObj["Transaction Processed At"],
        "currency": "USD",
        "device_id": null,
        "error_code": null,
        "gateway": "bogus",
        "kind": "capture",
        "location_id": null,
        "message": null,
        "order_id": orderObj["Name"],
        "parent_id": (parent_id),
        "processed_at": orderObj["Transaction Processed At"],
        "receipt": {
          
        },
        "source_name": "web",
        "status": "success",
        "test": false,
        "user_id": null
      }
    ],
  }
  
  return element
}

let formatLineItemObj = (lineItemObj) => {
  let lineItemUniqueId = addOneNumberINTransactionId();

  let element = {
    "id": lineItemUniqueId,
    "admin_graphql_api_id": `gid://shopify/LineItem/${lineItemUniqueId}`,
    "fulfillable_quantity": 0,
    "fulfillment_service": "manual",
    "fulfillment_status": lineItemObj["Lineitem fulfillment status"],
    "gift_card": false,
    "grams": 0,
    "name": lineItemObj["Lineitem name"],
    "price": lineItemObj["Lineitem price"],
    "price_set": {
      "shop_money": {
        "amount": lineItemObj["Lineitem price"],
        "currency_code": "USD"
      },
      "presentment_money": {
        "amount": lineItemObj["Lineitem price"],
        "currency_code": "USD"
      }
    },
    "product_exists": true,
    "product_id": null,
    "properties": [

    ],
    "quantity": lineItemObj["Lineitem quantity"],
    "requires_shipping": true,
    "sku": lineItemObj["Lineitem sku"],
    "taxable": true,
    "title": lineItemObj["Lineitem name"],
    "total_discount": "0.00",
    "total_discount_set": {
      "shop_money": {
        "amount": "0.00",
        "currency_code": "USD"
      },
      "presentment_money": {
        "amount": "0.00",
        "currency_code": "USD"
      }
    },
    "variant_id": lineItemObj["Lineitem variant id"],
    "variant_inventory_management": "shopify",
    "variant_title": lineItemObj["Lineitem variant title"],
    "vendor": "Fresh Meal Plan",
    "tax_lines": [

    ],
    "duties": [

    ],
    "discount_allocations": [

    ]
  }

  if (element.variant_title == "XL" && element.price == "4") {
    element.price = "15.99";
  }

  return element
}

let checkMainOrderObj = (data) => {
  let count = 0;
  if (data.hasOwnProperty("Email")) count++
  if (data.hasOwnProperty("Currency")) count++
  if (data.hasOwnProperty("Note")) count++
  if (data.hasOwnProperty("Billing Address1")) count++
  if (data.hasOwnProperty("Shipping Address1")) count++

  return count > 2 ? true : false;
}

let addOneNumberINTransactionId = () => {
  return UniqueTransactionId++
}